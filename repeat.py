from selenium import webdriver
#from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
import time

options = webdriver.ChromeOptions()
options.binary_location = 'C:\Program Files (x86)\Google\Chrome\Application\chrome.exe'
driver = webdriver.Chrome(chrome_options=options)
#url = driver.get('https://www.youtube.com/watch?v=OmGByG1FKhQ')
url = driver.get('https://youtu.be/sZCuS8CvAsI')


def page():
    u = str('https://youtu.be/sZCuS8CvAsI')
    u = driver.get(u)
    return u


def seconds(l):
    try:
        mi, se = [int(i) for i in l.split(':')]
        return int(60 * mi + se)
    except ValueError:
        pass


def wait_t():
    # implicit wait seconds before waiting duration
    imp = 5
    # if you try to skip through ads after only a few seconds, youtube will replay ads until you sit through one
    #ad = 31
    driver.implicitly_wait(2)
    dur = (driver.find_element_by_class_name('ytp-time-duration'))
    dt = dur.text
    ti = (seconds(dt))
    try:
        skip = WebDriverWait(driver, 15).until(EC.presence_of_element_located(driver.find_element_by_class_name\
                ("videoAdUiSkipButtonExperimentalText")))
        if skip is True:
            return page()
    except NoSuchElementException:
        pass
    except TypeError:
        pass
    #print("waiting " + str(ti) + " seconds")
    #if ti < int(ad):
    #    w = time.sleep(imp)
    #    return w
    try:
        ti = int(ti) - int(imp) + 1
        if ti == 0:
            wait = time.sleep(imp)
        elif ti < 0:
            wait = time.sleep(imp)
        else:
            try:
                wait = time.sleep(dt)
            except ValueError:
                wait = seconds(dt)
                wait = time.sleep(wait)
        return wait
    except TypeError:
        pass
    try:
        EC.presence_of_element_located(driver.find_element_by_class_name("ytd-mealbar-promo-renderer"))
    except NoSuchElementException:
        pass


while True:
    wait_t()
    try:
        current = (driver.find_element_by_class_name('ytp-time-current'))
        ct = current.text
        ms, ss = [int(i) for i in ct.split(':')]
        ct = 60 * ms + ss
        duration = (driver.find_element_by_class_name('ytp-time-duration'))
        d = duration.text
        print(type(d), type(ct))
        m, s = [int(i) for i in d.split(':')]
        d = 60 * m + s
        if int(d) > int(ct):
            pass
        elif d == ct:
            page()
            print("refreshing the page")
    except ValueError:
        pass
    except NameError:
        pass
    except TypeError:
        pass




        #button = (driver.find_element_by_class_name('videoAdUiSkipButton'))




#'<button class="ytp-play-button ytp-button" aria-label="Replay">'
#<div class="videoAdUiSkipButtonExperimentalText videoAdUiFixedPaddingSkipButtonText">Skip Ad</div>
#.until(EC.presence_of_element_located((By.CLASS_NAME, "ytp-play-button")))




